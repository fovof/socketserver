import os
import json
import urllib.request
import time
from subprocess import Popen, PIPE, call

def commandexec(deep):

	Error = "Выполнено без ошибок"

	if deep == "break":
		print("Выполняется завершение...")
		Code = 2

	elif deep == "reboot":
		p = Popen('reboot', shell=True, stdout=PIPE)
		out, err = p.communicate()
		err = p.returncode
		Code = 1

	elif deep == "poweroff":
		p = Popen('poweroff', shell=True, stdout=PIPE)
		out, err = p.communicate()
		err = p.returncode
		Code = 2

	elif 'update' in deep:
		print("Выполняется обновление...")
		try:
			list_update = deep.split()
			port = int(list_update[1])
			call(["wget","https://tomans.ru/socket/servercommand.py"])
			call(["wget","https://tomans.ru/socket/server"])
			Code = 3
			out = '\tИдет обновление...'.encode('utf-8')
			err = 0

		except Exception:
			Error = 'Ошибка обновления'
			Code = 0
			out = ''.encode('utf-8')
			err = 0

	else:
		Error = "Ошибка при считывании команды. Такой команды не существует. \n (servercommand.py, commandexec)"
		Code = 0
		out = ''
		err = 0

	return Error, Code, out, err

def trans(value):

	Error = "Выполнено без ошибок"
	err = 0
	out = ''

	try:
		pdo = json.loads(value,object_hook=do)
	except (LookupError, ValueError):                                            
		Error = 'Ошибка при считывании json. \n (servercommand.py, блок do)' 
		Code = 0
		pdo = None                       
	if pdo == "exec":
		try:
			commandout = json.loads(value,object_hook=command)
			p = Popen(commandout, shell=True, stdout=PIPE)
			out, err = p.communicate()
			err = p.returncode
			Code = 1
		except (LookupError, ValueError):
			Error = 'Ошибка при считывании json. \n (servercommand.py, pdo == "exec", блок command)'
			Code = 0       

	elif pdo == "inside":
		try:
			commandin = json.loads(value,object_hook=command)
			Error, Code, out, err = commandexec(commandin)
		except (LookupError, ValueError):
			Error = 'Ошибка при считывании json. \n (servercommand.py, pdo == "inside", блок command)'
			Code = 0 

	elif pdo == None:
		None                       

	else:
		print('Ошибка при считывании блока do. Неизвестная команда ', pdo)
		Error = 'Ошибка при считывании блока do. Неизвестная команда ' + pdo + '\n (servercommand.py, trans)'
		Code = 0

	return Error, Code, out, err

#Функции для форматирования
def do(comm):
	return comm['do']

def command(comm):
	return comm['command']

def param(comm):
	return comm['param']

def TOTP(comm):
	return comm['TOTP']
